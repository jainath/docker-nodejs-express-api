FROM node:8.11-alpine

# Install CURL which is used for healthcheck
RUN apk add --no-cache curl

# Set work directory
WORKDIR /usr/src/app

# copy all our source code into the working directory
COPY . .

# Install pm2 to manage the node process
RUN npm install pm2@2 -g

# Download application dependencies
RUN npm install

# Expose application port
EXPOSE 3000

# Expose application debug port
EXPOSE 9232

# start node.js application with pm2
CMD ["pm2", "start", "processes.json", "--no-daemon"]